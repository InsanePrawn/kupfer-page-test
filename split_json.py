#!/usr/bin/env python

import click
import json
import logging
import os


@click.group('splitter')
def cmd_main():
    pass


@cmd_main.command('keys', help='Print keys in a JSON dictionary')
@click.argument('json_file', type=click.File())
def cmd_keys(json_file: click.File, output_dir: str = None):
    with json_file as fd:
        json_data = json.load(fd)
    for k in json_data.keys:
        print(k)


@cmd_main.command('split', help="Split JSON dictionary into files for each key")
@click.option('-o', '--output-dir', type=click.Path(exists=False))
@click.option('-e', '--output-extension', default='.json')
@click.option('--subdir-indexes', help="Create a subdirectory per key, with index files instead of neighbouring json files", is_flag=True)
@click.option('--index-name', default='index.html', show_default=True)
@click.option('--index-content', help='Override the content of subdir index files if created. Variables "{name}" and "{data}" supported.')
@click.argument('json_file', type=click.File())
def cmd_split(
    json_file: click.File,
    output_dir: str = None,
    output_extension: str = '.json',
    subdir_indexes: bool = False,
    index_name: str = 'index.html',
    index_content: str | None = None,
):
    with json_file as fd:
        json_data = json.load(fd)
    output_dir = output_dir or os.path.dirname(json_file.name)
    assert isinstance(json_data, dict)
    for name, data in json_data.items():
        filename = f'{name}/{index_name}' if subdir_indexes else f'{name}{output_extension}'
        if subdir_indexes:
            os.makedirs(os.path.join(output_dir, os.path.dirname(filename)), exist_ok=True)
        out_file = os.path.join(output_dir, filename)
        logging.info(f"Writing to {out_file}")
        with open(out_file, 'w') as fd:
            content = (index_content if index_content is not None else "{data}").replace("{name}", name)
            if "{data}" in content or content is None:
                json_str = json.dumps(data)
                content = (content or "{data}").replace("{data}", json_str)
            fd.write(content)


if __name__ == '__main__':
    cmd_main()

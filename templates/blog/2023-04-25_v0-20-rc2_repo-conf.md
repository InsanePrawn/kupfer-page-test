---
title: v0.2.0-rc2: Repo Configs and voice calls!
date: 2023-04-25
author: Prawn
---

Hello people of the internetz,

I've merged a bunch of work into [Kupferbootstrap](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.2.0-rc2) and [PKGBUILDs](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.2.0-rc2) `dev` as `v0.2.0-rc2`!

If you're on `dev` and have missed the [rc0 release](2022-11-12_v0-2-0rc0_merged.html), please go read up on the [migration instructions](2022-11-12_v0-2-0rc0_merged.html#migration-instructions).<br>
The [instructions for existing installations](2022-11-12_v0-2-0rc0_merged.html#migrating-your-existing-phone-installation-to-v02rc0) have been updated to utilize `kupfer-config --user apply`, but more on that later.

## What's new in v0.2-rc2

I think the biggest ticket item in this update will be the voice call support for SDM845.

### A few quick notes on voice calls

1. To use voice calls after upgrading your packages, make sure you've run `kupfer-config --user apply` first and rebooted. **Note the `--user`!**
1. **If your audio breaks, try this little script.** Do not skip steps, do not pass "go", do not collect $200.
  ```sh
  rm -rf ~/.config/pulse
  sudo systemctl stop alsa-restore
  sudo rm /var/lib/alsa/asound.state
  sudo reboot
  ```
  Thanks to Phosh's Guido for pointing me to this workaround!
1. Symptoms of the currently known SDM845 audio breakage:
    - Audio settings shows only earpiece, no speaker. Earpiece works though
    - Microphone doesn't work
    - No audio profile dropdown in settings (shows HiFi and Voicecall usually)

    This is due to pulseaudio failing to load the HiFi.conf UCM config due to allegedly missing devices.<br>
    No, switching to pipewire doesn't fix this.<br>
    pmOS is unaffected by this for unknown reasons.<br>
    The alsa-restore service seems to be involved, but disabling it doesn't always stop the problem from happening, it becomes rare though. We're shipping a config to neuter the service until this is diagnosed and fixed.

### Kupferbootstrap

[Kupferbootstrap v0.2.0-rc2](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.2.0-rc2)

- Progress bars during scanning and building packages
- Colours and JSON output for the `devices` and `flavours` subcommands
- The [Website](../../index.html) now contains browsable lists of [Flavours](/flavours/index.html), [Devices](/devices/index.html) and [Packages](/packages/index.html), powered by the new `--json` flag
- [`config init`](https://kupfer.gitlab.io/kupferbootstrap/v0.2.0-rc2/cli/config/index.html#kupferbootstrap-config-init) fix: Fixes [KBS Issue #29: "`config init` doesn't write config file due to docker wrapper"](https://gitlab.com/kupfer/kupferbootstrap/-/issues/29) that slipped into rc1
- Support for overriding the binary repos via a new `pkgbuilds/repos.yml` file.
  Also added a new local-only repo. See the `repos.yml.default` in the updated `pkgbuilds`!
- [`image build`](https://kupfer.gitlab.io/kupferbootstrap/v0.2.0-rc2/cli/image/index.html#kupferbootstrap-image-build) now uses `kupfer-config --user` to enable systemd user services for voicecalls


### PKGBUILDs

[PKGBUILDs v0.2.0-rc2](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.2.0-rc2)

- [**Phosh and friends**: 0.26](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/ad690911a2415d820c0b9b5eeb5f0722fd286ef0): Phosh now looks more in line with Gnome 42's design, **phosh-osk-stub gained completion suggestions**! Also SDM845 devices should have notification LED and less aggressive vibration feedback now.
- [**firefox-mobile-config**: 4.0.0](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/8aacd36244a9c42226ebcfada1f50157c5a80bae): Fixes the issue with page contents being unresponsive to touches with newer firefox versions, which made firefox completely unusable.
- [**kupfer-config**: 0.3](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/335cf73d8d09122757b763eec9474c3a046b4a69): add `--user` to manage systemd user services, rename config files to `.lst`
- [**pixman**: 0.42.2](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/78d3d67b78f0fc3dd38753c216ef9dd2ad54cc43#05ec5029e4116733d651caa9fc0ff2782a395413): Upstream now contains the aarch64 vector patch and fixed a CVE
- [**mkinitcpio-busybox:**: 1.36.0 and musl](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/4233d626d0011115475770c6f0e1a32966a71940): Busybox needs musl to build statically now, but on the bright side this allowed to us to move forward on making it build for all architectures, removing one of the last few roadblocks for proper multiarch and armv7h.
- [**bootmac**: new package](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/e35b111bec48617d70869af148340d279a8ce39c): Port of the pmOS package to generate a semi-static MAC address from hostid on our SDM845 phones to avoid your WiFi IP always changing and other fun problems. Thanks to raihan2000!
- [**linux-sdm845**: 6.2](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/c1f51fb658f456559faab8d7339db447d77194e4): This kernel gives the basis to enable voice calls on all three currently supported Kupfer SDM845 devices. Thanks Syboxez!
- [**q6voiced**: new package](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/0b28181192b7ce16142177c4991d14a82a9f7206): Daemon needed for voice calls on our Snapdragon devices. Thanks uocboxer!
- [**gnome-calls**: new package](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/84ef2470ee06daf3043929c62b9884c57140e7ce): Gotta have calls to make calls.
- **device-sdm845-\***: 0.4: Update a bunch of configs for voice calls

\+ [a bunch more updates](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/78d3d67b78f0fc3dd38753c216ef9dd2ad54cc43) from Syboxez as usual!

## Coming up:

- Keyrings and signature verification (prototyping in progress)
- Disk Encryption
    - finally merge the `initsquared` shim initramfs package
    - add some OSK hook. which OSK is still TBD
- Switch to postmarketOS' [boot-deploy](https://gitlab.com/postmarketOS/boot-deploy) for building android boot.img (no change for users, just replacing homebrewed utils with pmos upstreams)

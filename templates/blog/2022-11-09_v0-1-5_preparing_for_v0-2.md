---
title: v0.1.5, Preparing for v0.2.0rc0
date: 2022-11-09 03:20:00
author: Prawn
---

Hi there!

I've tagged **v0.1.5** with the changes that happened since the last release and pushed it to the main branch.

I'll soon merge my oversized [Kupferbootstrap](https://gitlab.com/kupfer/kupferbootstrap/-/merge_requests/27) and [PKGBUILDs](https://gitlab.com/kupfer/packages/pkgbuilds/-/merge_requests/67) merge requests into `dev`; somewhere between the evenings (CET) of Thursday (2022-11-10) and Friday (2022-11-11).

As they will form the basis for v0.2.0, I will be calling them v0.2.0rc0.

I'm announcing this early to give people some time to migrate to `main` before blindly updating themselves into half a `dev` installation.

We'll both show you how to migrate to `main` in order to stay on v0.1.5 and give you a preview of what's new in v0.2.0rc0, right after the usual release notes for v0.1.5.

[TOC]

## Kupfer v0.1.5

Kernel 6.0, phosh was rebuilt since gnome got updated to 43, phosh-mobile-configs got packaged by Syboxez!

### kupferbootstrap

[kupferbootstrap v0.1.5](https://gitlab.com/kupfer/kupferbootstrap/-/tags/v0.1.5)

Literally no changes.


### PKGBUILDS

[pkgbuilds v0.1.5](https://gitlab.com/kupfer/packages/pkgbuilds/-/tags/v0.1.5)

- [**phosh/phosh-mobile-settings: New package**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/ce6f96ef5968a96e9c518a7e35d6f463550a8f7c)
- [**linux/sdm845: Update to 6.0.3**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/b7bd322f54d4d76c4c69add744aedb7ffb1a4637)
- [**linux/msm8916: Update to 6.0.2**](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/9fe8e114a158ef38166495115d92d240cb0997b3): Looking for testers! If you have a bq paella, contact us on [Matrix](https://matrix.to/#/#kupfer:matrix.org)!
- **bumped pkgver for [phoc](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/bc59ed006a974b3a749a6cee64c254ba785a2fc0) and [phosh and phosh-osk-stub](https://gitlab.com/kupfer/packages/pkgbuilds/-/commit/05281d1881642923e36b7ed37ca37ad820d154a5)**: Rebuild against Gnome 43.

**Thanks to Syboxez for packaging phosh-mobile-settings and updating the kernels!**

## Migration Instructions

So, without further ado, let me present you with your two options:

2. Don't want to be the victim of my development whims so much? Want to postpone the migration pain until v0.2.0 is ready and fully released? Prefer smooth-ish sailing? Understandable.
<br>You can stay on v0.1.x for now by [switching to the `main` branch](#migrating-to-the-main-branch) (if you're unsure, read the linked instructions on how to change branches - you'll see what you have). You can already do this today, before v0.2.0rc0 hits `dev`!
1. Or: Take a ride, experience and test the new Kupfer(bootstrap) with us. Unlike previous versions, updating will require some manual interventions.
<br>If you're up for it, wait for the release blog post with the migration instructions for v0.2rc0 in the next two days.
<br>Sidenote: After v0.2.0 (proper) is released to `main`, you will have to do this anyway, but more things might change along the way; `dev` is short for development after all.

### Migrating to the `main` branch

Updating to v0.1.5 is as easy as switching to the newest version of the `main` branch in both `kupferbootstrap.git` and `pkgbuilds.git`.

**Step by step:**

1. Open a terminal.
1. Edit your config file:
    1. Open your config file with your favourite editor, e.g. `nano ~/.config/kupfer/kupferbootstrap.toml`.
         <br>If that file doesn't exist or is empty for some reason, run `kupferbootstrap config init -N` **now** and reopen the file.
         <br>It should contain the default values now.
    1. In the `[pkgbuilds]` section, set `git_branch` to `"main"`
    1. in the `[pacman]` section, set `repo_branch` to `"main"`
    ```toml
    [pkgbuilds]
    ...
    git_branch = "main"

    [pacman]
    ...
    repo_branch = "main"
    ```
1. Change directory (`cd`) into your **`kupferbootstrap`** directory, then run the following commands:
```shell
git remote update
git switch -f main
git reset --hard origin/main
```
1. Make sure all the python dependencies from `requirements.txt` are installed. Either consult the AUR or [create a venv and] run `pip install -r requirements.txt`
1. Change directory into your **`pkgbuilds`** directory (`~/.cache/kupfer/pkgbuilds` by default) and run the commands from the last step here too.
1. If all of that succeeded, back up and regenerate your config, just in case: `cp ~/.config/kupfer/kupferbootstrap.toml{,.premigration} && kupferbootstrap config init -N`

#### Migrating your existing phone installation to `main`

On your device, edit your `pacman.conf`, e.g. `sudo nano /etc/pacman.conf`, best done from a PC with SSH.

1. Ensure your Kupfer repos towards the end of the file are pointing at `/main/`, not `/dev/`, like so:
    ```ini
    [boot]
    Server = https://gitlab.com/kupfer/packages/prebuilts/-/raw/main/$arch/$repo
    ```

    Change this for all the Kupfer repos.

1. After making it so, upgrade your system with `pacman -Syu`. You should be done.

## What's new in v0.2rc0

Here's the major changes you can look forward to, off the top of my head:

- KBS now runs as the **correct uid instead of root and uses `sudo`** to escalate when necessary.
  This mostly means that `pkgbuilds.git` will now be usable with regular git, inside and outside of Docker, also from your regular terminal.
- Speaking of **Docker**: `kupferbootstrap` now works well without the Docker wrapper on Arch Linux hosts, with one little caveat: it might install qemu-user-static and binfmt-qemu on your host.
    - While I'm not gonna guarantee that it's *not* gonna eat your house and burn down your homework, I use it without the wrapper a lot, and my filesystem is still here. It really speeds things up.<br>
      You can either call KBS with the `-W / --no-wrapper` flag or set `type = "none"` in the `[wrapper]` section of your KBS config.
    - Using `--no-wrapper` also makes the `net ssh` and `packages sideload` commands much more useful, since ssh-agent passthrough into Docker isn't implemented.
    - KBS is now a little more lax on what conditions have to be met before it wraps, in the interest of speed.
    - During development, I've found it helpful to also introduce the symmetrical `-w / --force-wrapper` flag to force KBS to wrap in docker even when it hasn't yet or wouldn't have decided that it needs to wrap.
- **Package-parsing and -building** has been updated a lot. It should be faster and smarter (about when building is needed) than ever.
  <br>Over the last few months, KBS has learned:
    - how to download packages instead of building them if the right version is available
    - to cache the SRCINFO (`.srcinfo_meta.json`), because even a lot of parallel calls to `makepkg --printsrcinfo` take kinda long if you run them **every. single. execution.** of KBS.
    - to know whether it needs to `prepare()` sources or not (`srcinfo_initialised.json`)
    - how to parse and handle split packages properly
    - `_mode=` is no longer necessary to build a PKGBUILD, making it easier to locally drop in 3rd party PKGBUILDs. `kupferbootstrap packages check` will still fail on this though, so that we keep it explicit in our official Kupfer pkgbuilds.git.
    - similar to `_mode=`, we've introduced a new optional `_nodeps=true` option in PKGBUILDs, to mark a package only needs their `makedepends` during build time, speeding up builds of packages such as metapackages or configs.
- **Devices** and **Flavours** are no longer hardcoded into the KBS source code. They are now read from packages in the PKGBUILDs, which is a big part of the changes in pkgbuilds.git.
    - You can now print them with `kupferbootstrap devices` and `Kupferbootstrap flavours` respectively.
    - Attributes like the storage sector size are now parsed from the devices' deviceinfo file.
    - To enable systemd services for Flavours (and Devices, think ModemManager or qbootctl), we have introduced `kupfer-config`, a simple script to enable a bunch of services en masse, as specified by a number of packaged files, user overrideable. I'll write some more info on it in a future post.
